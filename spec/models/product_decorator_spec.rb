require 'rails_helper'

RSpec.describe "Potepan::ProductDecorator", type: :model do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  it "get related products" do
    expect(product.related_products).to include related_product
  end

  it "exclude product" do
    expect(product.related_products).not_to include product
  end

  it "have no same product in related_products array" do
    expect(product.related_products).to eq product.related_products.uniq
  end
end
