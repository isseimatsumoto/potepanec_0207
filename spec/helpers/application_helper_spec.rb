require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Application Title helpers" do
    it "Title is displayed" do
      expect(full_title("RUBY ON RAILS TOTE")).to eq "RUBY ON RAILS TOTE | BIGBAG Store"
      expect(full_title("")).to eq "BIGBAG Store"
    end

    it 'page_title is nil' do
      expect(full_title(nil)).to eq "BIGBAG Store"
    end
  end
end
