require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, name: "bag", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon]) }
  let!(:other_product) { create(:product) }

  context "category taxon page" do
    before do
      visit potepan_category_path(taxonomy.root.id)
    end

    scenario "View category show page" do
      expect(page).to have_title "#{taxonomy.root.name} | BIGBAG Store"
      expect(page).to have_link "Home", href: potepan_index_path
      within '.page-title' do
        expect(page).to have_content taxonomy.name
      end
    end

    scenario "should be able to visit category" do
      expect(page).to have_link taxon.name, href: potepan_category_path(taxon.id)
    end

    scenario "should be able to visit products" do
      expect(page).to have_link product.name, href: potepan_product_path(product.id)
    end
  end

  context "bag taxon page" do
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario "Products for each category are displayed" do
      expect(current_path).to eq potepan_category_path(taxon.id)
      expect(page).to have_title "#{taxon.name} | BIGBAG Store"
      within '.page-title' do
        expect(page).to have_content taxon.name
      end
    end

    scenario "link to product detail page" do
      click_on taxon.name
      expect(page).to have_content product.name
      expect(page).not_to have_content other_product.name
    end
  end
end
