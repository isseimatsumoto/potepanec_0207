require 'rails_helper'

RSpec.feature "Products_feature", type: :feature do
  let(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:un_related_product) { create(:product) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "View products page" do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} | BIGBAG Store"
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
    within '.products-info' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  scenario "product name and price of related products are displayed" do
    within(".productBox") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario "click related product to move to product details page" do
    click_on related_product.name
    expect(page).to have_current_path potepan_product_path(related_product.id)
  end

  scenario "unrelated product are not displayed" do
    within(".productBox") do
      expect(page).not_to have_content un_related_product.name
    end
  end
end
