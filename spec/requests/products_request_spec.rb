require 'rails_helper'

RSpec.describe "Products_request", type: :request do
  describe "GET #show" do
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_url(product.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to have_http_status 200
    end

    it "show templete products display" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it "4 related products are displayed" do
      expect(controller.instance_variable_get("@related_products").size).to eq 4
    end
  end
end
