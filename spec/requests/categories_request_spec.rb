require 'rails_helper'

RSpec.describe "Categories_request", type: :request do
  describe "GET categories#show" do
    let(:category) { create(:taxonomy, name: "Category") }
    let(:bag) { create(:taxon, name: "Bag", taxonomy: category) }
    let!(:rails_bag) { create(:product, name: "Rails Bag", taxons: [bag]) }

    before do
      get potepan_category_path(bag.id)
    end

    it "responds successfully and 200 response" do
      expect(response).to have_http_status(200)
    end

    it "assigns instance variables" do
      expect(controller.instance_variable_get("@taxonomies")).to match_array category
      expect(controller.instance_variable_get("@taxon")).to eq bag
      expect(controller.instance_variable_get("@products")).to match_array rails_bag
    end
  end
end
